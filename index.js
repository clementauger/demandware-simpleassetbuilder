#!/usr/bin/env node

var pkg = require('./package.json');
var _ = require("underscore");
var path = require("path");
var fs = require("fs-extra");
var which = require("which");
var chalk = require("chalk");
var async = require("async");
var temp = require('temp');
var symbols = require('symbolsjs');
var Spinner = require('cli-spinner').Spinner;
var parser = require('cline-parser');
var spawn = require('child_process').spawn;

var program = require('commander');
var versionFileParser = require('demandware-versionfileparser');
var debug = require("debug")(pkg.name);

program
  .version(pkg.version)
  .option('-v, --verbose', 'Increase verbosity.')
  .option('-p, --path <path>', 'CWD path the program will work in.')
  .option('-o, --only <only>', 'css|js')
  .option('-c, --config <config>', 'Configuration file name, default to dev')
  .parse(process.argv);

temp.track();

var npmPath         = whichNpm()          || process.exit(1);
var watchyPath      = whichWatchy()       || process.exit(1);
var browserifyPath  = whichBrowserify()   || process.exit(1);
var sassPath        = whichScss()         || process.exit(1);
var exorcistPath    = whichExorcist()     || process.exit(1);
var requestPath     = whichRequest()      || process.exit(1);
var postcssPath     = whichPostCss()      || process.exit(1);
var uglifyjsPath    = whichUglify()       || process.exit(1);

var verbose = !!program.verbose;
var only    = program.only || null;
var cwdPath = program.path || process.cwd();
var config  = program.config || 'dev';

symbols.ok = '   '+symbols.ok;

if(fs.existsSync(cwdPath) === false ){
  console.error('This path does not exists.\n%s', cwdPath);
  if(err) return console.error( chalk.red(symbols.err) + 'This path does not exists.\n%s', cwdPath )
  process.exit(1);
}

var vmOptsPath = path.join(cwdPath, '.auth.json');
var vmOpts = loadAuthFile(vmOptsPath) || process.exit(1);


var binVars = {
  projectPath: cwdPath,
  thisBinPath: __dirname,
  vmOpts: vmOpts,
  version: '',
  vmBaseUrl: 'https://<%=vmOpts.user%>:<%=vmOpts.password%>@<%=vmOpts.host%>/on/demandware.servlet/webdav/Sites/Cartridges/<%=version%>',

  js:{
    watch: "OPI_richUI/cartridge/js/,package.json",
    source: "OPI_richUI/cartridge/js/app.js",
    target: "OPI_richUI/cartridge/static/default/js/app.js",
    targetMap: "<%=js.target%>.map",
    tempPath: ""
  },

  css:{
    watch: "OPI/cartridge/scss/,package.json",
    source: "OPI/cartridge/scss/default/style.scss",
    target: "OPI/cartridge/static/default/css/style.css",
    targetMap: "<%=css.target%>.map",
    tempPath: ""
  }
};


var binOpts = {
  scss: '--sourcemap=auto --style=compressed <%=projectPath%>/<%=css.source%> <%=projectPath%>/<%=css.target%>',
  autoprefix: '--use autoprefixer --autoprefixer.browsers "last 2 versions, ie > 7" -o <%=projectPath%>/<%=css.target%> <%=projectPath%>/<%=css.target%>',

  browserify: '<%=projectPath%>/<%=js.source%> -t [ <%=thisBinPath%>/node_modules/aliasify --configDir <%=projectPath%>] --debug -o <%=js.tempPath%>',
  exorcist: '<%=js.tempPath%>.map -o <%=js.tempPath%>.tmp -i <%=js.tempPath%> -u <%=path.basename(js.tempPath)%>.map',
  uglify: '<%=js.tempPath%>.tmp -v --mangle --source-map <%=projectPath%>/<%=js.targetMap%> --in-source-map <%=js.tempPath%>.map --source-map-url <%=path.basename(js.targetMap)%> -o <%=projectPath%>/<%=js.target%>',

  push: '<%=vmBaseUrl%>/<%=currentPath%> -T <%=projectPath%>/<%=currentPath%>'
};

var processes = [];
process.on('exit', function() {
  processes.forEach(function(p){
    process.kill(p.pid);
  })
});


var activeVersion = 'version1';

async.parallel([
  function ensurePackageJsonIsCorrect(done){
    console.error( chalk.cyan('Checking your setup') )
    checkPackageFile(cwdPath, function(err){
      if(!err) console.error( chalk.green(symbols.ok) + ' All good ');
      done(err)
    });
  },
  function ensureGitignoreIsFine(done){
    console.error( chalk.cyan('Fixing ignored files, just in case.') )
    var gitgnorePath = path.join(cwdPath, '.gitignore');
    fileAppend(gitgnorePath, '.auth.json')
    fileAppend(gitgnorePath, '.sass-cache')
    fileAppend(gitgnorePath, 'node_modules')
    console.error( chalk.green(symbols.ok) + ' Ready to go');
    done();
  },
  function fetchActiveVersion(done){
    var spinner = new Spinner( chalk.cyan('Fetching active version')+' %s' );
    spinner.setSpinnerString(Spinner.spinners[0]);
    if(!verbose) spinner.start();
    fetchVersionFile(vmOpts, function(err, foundVersion){
      if(!verbose) spinner.stop(), console.log('\n');
      if(!err) console.error( chalk.green(symbols.ok) + ' Active version: ' + foundVersion);
      activeVersion = foundVersion;
      binVars.version = foundVersion || activeVersion;
      done(err);
    });
  }
], function startWatchy(err){
  if(err) return console.error( chalk.red(symbols.err) + ' %s', err )

  var projectPkg = require( path.join(cwdPath, 'package.json') );

  binVars = extendConfig(binVars, projectPkg.build ? projectPkg.build[config].vars : {});
  binOpts = extendConfig(binOpts, projectPkg.build ? projectPkg.build[config].bin : {});

  binVars.css.tempPath = temp.path({suffix: '.whatever'});
  binVars.js.tempPath = temp.path({suffix: '.whatever'});
  binVars = templatizeConfig(binVars);
  debug(binVars)
  binVars.path = path;

  if(only!=='css'){
    var jsP = (binVars.js.watch || path.dirname(binVars.js.source));
    console.error( chalk.gray('watching js ' + jsP));
    watchThis(jsP, function(){
      transformJSFile(binVars, function(){});
    });
  }
  if(only!=='js'){
    var cssP = (binVars.css.watch || path.dirname(binVars.css.source));
    console.error( chalk.gray('watching css ' + cssP));
    watchThis(cssP, function(){
      transformCSSFile(binVars, function(){});
    });
  }
});


function spawnIt(p, a, opts){
  debug(p+' '+a.join(' '));
  opts = opts || {};
  opts.cwd = cwdPath;
  var child = spawn(p, a, {cwd:cwdPath});
  processes.push(child);
  child.on('close', function(){
    processes = _.without(processes, child);
  });
  return child;
}

function extendConfig(dest, src){
  for(var k in dest){
    if(src[k]){
      if( !_.isString(dest[k]) ){
        dest[k] = extendConfig(dest[k], src[k]);
      }else{
        dest[k] = src[k];
      }
    }
  }
  for(var k in src){
    if( !dest[k] ){
      dest[k] = src[k];
    }else if( !_.isString(dest[k]) ){
      dest[k] = extendConfig(dest[k], src[k]);
    }
  }
  return dest;
}

function templatizeConfig(config, data){
  data = data || config;
  Object.keys(config).forEach(function(k){
    if(_.isString(config[k])){
      config[k] = _.template(config[k])(data);
    }else if( !_.isFunction(config[k]) ){
      config[k] = templatizeConfig(config[k], config);
    }
  });
  return config;
}

function transformJSFile(binVars, then){

  console.error( chalk.cyan('browserifying ')+' ...');
  browserifyIt(binVars, function(err){
    if(err) return console.error(chalk.red('ERROR')+' Build failed, please check your JS files.\n');
    console.error( chalk.cyan('exorcizing ')+' ...');
    exorcizeThat(binVars, function(){
      uglifyIt(binVars, function(){
        var spinner = new Spinner( chalk.cyan('pushing js files')+' %s' );
        spinner.setSpinnerString(Spinner.spinners[0]);
        if(!verbose) spinner.start();
        async.parallel([
          function(done){pushToRemote( binVars, binVars.js.target, done)},
          function(done){pushToRemote( binVars, binVars.js.targetMap, done)}
        ], function(err){
          if(!verbose) spinner.stop(), console.error('\n');
          console.error(chalk.green(symbols.ok)+' js files pushed.')
          then(err)
        });
      });
    });
  });
}

function transformCSSFile(binVars, then){

  console.error( chalk.cyan('sass-ing ')+' ...');
  scssThat(binVars, function(err){
    if(err) return console.error(chalk.red('ERROR')+' Build failed, please check your CSS files.');
    console.error( chalk.cyan('prefix-ing ')+' ...');
    autoprefixIt(binVars, function(){
      var spinner = new Spinner( chalk.cyan('pushing css files')+' %s' );
      spinner.setSpinnerString(Spinner.spinners[0]);
      if(!verbose) spinner.start();
      async.parallel([
        function(done){pushToRemote( binVars, binVars.css.target, done)},
        function(done){pushToRemote( binVars, binVars.css.targetMap, done)}
      ], function(err){
        if(!verbose) spinner.stop(), console.error('\n');
        console.error(chalk.green(symbols.ok)+' css files pushed.')
        then(err)
      });
    });
  });
}


function watchThis(watchPath, then){
  var watchy = spawnIt(watchyPath, ['-w', watchPath, '--', 'echo', 'okgo']);
  if(verbose){
    watchy.stdout.pipe(process.stdout);
    watchy.stderr.pipe(process.stderr);
  }
  then = _.debounce(then, 3000, true);
  watchy.stdout.on('data', function(d){
    if(d+''.match(/okgo/)){
      then();
    }
  });
  return watchy;
}

function scssThat(binVars, then){

  var scssCmd = sassPath + ' ' + _.template(binOpts.scss)(binVars);
  scssCmd = parser(scssCmd)

  var scss = spawnIt(scssCmd.prg, scssCmd.args);
  var err;
  scss.stdout.pipe(process.stdout);
  scss.stderr.pipe(process.stderr);
  scss.stderr.on('data', function(d){
    d = '' + d;
    if(d.match(/^Error:/) ){
      err = d+'\n';
    }else if(err){
      err += d+'\n';
    }
  })
  scss.on('close', function(){
    then(err);
  });
  return scss;
}

function autoprefixIt(binVars, then){

  var autoprefixCmd = postcssPath + ' ' + _.template(binOpts.autoprefix)(binVars);
  autoprefixCmd = parser(autoprefixCmd)

  var postCSS = spawnIt(autoprefixCmd.prg, autoprefixCmd.args);
  postCSS.stdout.pipe(process.stdout);
  postCSS.stderr.pipe(process.stderr);
  postCSS.on('close', function(){
    then();
  });
  return postCSS;
}

function browserifyIt(binVars, then){

  var brifyCmd = browserifyPath + ' ' + _.template(binOpts.browserify)(binVars);
  brifyCmd = parser(brifyCmd)
  var brifyArgs = brifyCmd.args;

  var err;
  var brify = spawnIt(brifyCmd.prg, brifyArgs);
  brify.stdout.pipe(process.stdout);
  brify.stderr.pipe(process.stderr);
  brify.stderr.on('data', function(d){
    d = '' + d;
    if(d.match(/^Error:/) || d.match(/^SyntaxError:/) ){
      err = d+'\n';
    }else if(err){
      err += d+'\n';
    }
  });
  brify.on('close', function(){
    then(err);
  });
  return brify;
}

function exorcizeThat(binVars, then){

  var exorcistCmd = exorcistPath + ' ' + _.template(binOpts.exorcist)(binVars);
  exorcistCmd = parser(exorcistCmd)
  var exorcistArgs = exorcistCmd.args;

  var err;
  var exorcist = spawnIt(exorcistCmd.prg, exorcistArgs);
  exorcist.stdout.pipe(process.stdout);
  exorcist.stderr.pipe(process.stderr);
  exorcist.stderr.on('data', function(d){
    err += d+'\n';
  });
  exorcist.on('close', function(){
    then(err);
  });

  return exorcist;
}

function uglifyIt(binVars, then){

  var uglifyCmd = uglifyjsPath + ' ' + _.template(binOpts.uglify)(binVars);
  uglifyCmd = parser(uglifyCmd)

  var err;
  var uglify = spawnIt(uglifyCmd.prg, uglifyCmd.args);
  uglify.stdout.pipe(process.stdout);
  uglify.stderr.pipe(process.stderr);
  uglify.stderr.on('data', function(d){
    d = '' + d;
    if(d.match(/^Error:/) ){
      err = d+'\n';
    }else if(err){
      err += d+'\n';
    }
  });
  uglify.on('close', function(){
    then(err);
  });
  return uglify;
}




function pushToRemote(binVars, filePath, then){
  var t = _.clone(binVars);
  t.currentPath = filePath;
  var reqCmd = requestPath + ' ' + _.template(binOpts.push)(t);
  reqCmd = parser(reqCmd)

  var req = spawnIt(reqCmd.prg, reqCmd.args);
  if(verbose){
    req.stdout.pipe(process.stdout);
    req.stderr.pipe(process.stderr);
  }
  req.on('error', function(err){
    console.error( chalk.yellow('!') + ' Failed to transfer files' );
    console.error( err );
    then(err);
  });
  req.on('close', function(){
    if(then) then(null);
  });
  return req;
}

function fetchVersionFile(vmOpts, then){
  var url = 'https://'+vmOpts.user+':'+vmOpts.password+'@'+vmOpts.host+'/on/demandware.servlet/webdav/Sites/Cartridges/.version';
  var req = spawnIt(requestPath, [url, '--body', '--raw']);
  if(verbose){
    req.stdout.pipe(process.stdout);
    req.stderr.pipe(process.stderr);
  }
  var activeVersionFound;
  versionFileParser(req.stdout)
    .on('activeVersion', function(activeVersion){
      activeVersionFound = activeVersion;
    }).on('error', function(err){
      then(err);
    }).on('end', function(){
      if(!activeVersionFound){
        throw 'Could not find active version'
      }
      then(null, activeVersionFound);
    });
}


function loadAuthFile(vmOptsPath){
  if(fs.existsSync(vmOptsPath) === false ){

    if(err) return console.error( chalk.red(symbols.err) + 'Please update VM configuration file.\n%s', vmOptsPath )

    fs.writeFileSync(vmOptsPath, JSON.stringify({host:'', user:'', password:''}, null, 4));

    var gitgnorePath = path.join(cwdPath, '.gitignore');
    fileAppend(gitgnorePath, '.auth.json')
    return false;
  }
  return JSON.parse(fs.readFileSync(vmOptsPath));
}


function checkPackageFile(projectPath, then){
  var pkgFile = path.join(projectPath, 'package.json')
  if(fs.existsSync(pkgFile)===false){
    console.error( chalk.red(symbols.err) + ' It appears you do not have package json file on root of your directory')
    console.error( chalk.yellow('!') + ' I will create one for you and setup required dependencies for SiteGenesis')
    var npmArgs = ['init', '-y'];
    var npm = spawn(npmPath, npmArgs, {cwd:cwdPath});
    debug(npmPath+' '+npmArgs.join(' '));
    npm.stdout.pipe(process.stdout);
    npm.stderr.pipe(process.stderr);
    npm.stdout.on('data', function(d){
      d = '' + d;
      if( d.match(/author:/) ){
        npm.stdin.write('not set');
        npm.stdin.write('\n');
        npm.stdin.end();
      }
    });
    npm.on('close', function(){
      var npmIArgs = ['i', 'lodash', 'promise', 'imagesloaded', '--save'];
      var npmI = spawn(npmPath, npmIArgs, {cwd:cwdPath});
      debug(npmPath+' '+npmIArgs.join(' '));
      npmI.stdout.pipe(process.stdout);
      npmI.stderr.pipe(process.stderr);
      npmI.on('close', function(){
        console.error( chalk.yellow('!') + ' In the future you can use')
        console.error( chalk.yellow('!') + ' npm i [package name] --save')
        console.error( chalk.yellow('!') + ' to load new dependencies in your storefront')
        then();
      });
    });
    return npm;
  } else {
    console.error( chalk.cyan('Checking your dependencies') )
    var pkg = fs.readFileSync( path.join(pkgFile));
    pkg = JSON.parse(pkg);
    var missingDeps = _.difference(['lodash', 'promise', 'imagesloaded'], Object.keys(pkg.dependencies || {}));
    if(!missingDeps.length) {
      then();
    }else{
      console.error( chalk.yellow('!') + ' You are missing ' + missingDeps.join(' '))
      var npmIArgs = ['i'];
      npmIArgs = npmIArgs.concat(missingDeps);
      npmIArgs.push('--save')
      var npmI = spawn(npmPath, npmIArgs, {cwd:cwdPath});
      debug(npmPath+' '+npmIArgs.join(' '));
      npmI.stdout.pipe(process.stdout);
      npmI.stderr.pipe(process.stderr);
      npmI.on('close', function(){
        console.error('')
        console.error( chalk.yellow(symbols.ok) + ' Package done, It is set !')
        console.error('')
        then();
      });
    }
  }
}

function whichNpm(){
  try{
    return which.sync('npm');
  }catch(ex){
    console.error( chalk.red(symbols.err) + 'can not find `npm` on your PATH.')
    console.error('');
    console.error( chalk.yellow('!') + 'You should set up node and npm first, go at ');
    console.error( chalk.yellow('!') + 'http://nodejs.org');
    console.error('');

    return false;
  }
}

function whichScss(){
  try{
    return which.sync('sass');
  }catch(ex){
    console.error( chalk.red(symbols.err) + 'can not find `sass` on your PATH.')
    console.error('');
    console.error( chalk.yellow('!') + 'You should set up ruby first, go at ');
    console.error( chalk.yellow('!') + 'http://rubyinstaller.org/downloads/');
    console.error( chalk.yellow('!') + '');
    console.error( chalk.yellow('!') + 'Be sure not to forget to install sass gem right after with ');
    console.error( chalk.yellow('!') + 'gem install sass');
    console.error('');

    return false;
  }
}

function whichUglify(){
  try{
    return which.sync('uglifyjs');
  }catch(ex){
    console.error( chalk.red(symbols.err) + 'can not find `uglifyjs` on your PATH.')
    console.error('');
    console.error( chalk.yellow('!') + 'You should set up node and npm first, go at ');
    console.error( chalk.yellow('!') + 'http://nodejs.org');
    console.error('');
    console.error( chalk.yellow('!') + 'then you can type in ');
    console.error( chalk.yellow('!') + 'npm i -g uglify-js');
    console.error('');
    return false;
  }
}

function whichPostCss(){
  try{
    return which.sync('postcss');
  }catch(ex){
    console.error( chalk.red(symbols.err) + 'can not find `postcss` on your PATH.')
    console.error('');
    console.error( chalk.yellow('!') + 'You should set up node and npm first, go at ');
    console.error( chalk.yellow('!') + 'http://nodejs.org');
    console.error('');
    console.error( chalk.yellow('!') + 'then you can type in ');
    console.error( chalk.yellow('!') + 'npm i postcss-cli autoprefixer -g');
    console.error('');

    return false;
  }
}

function whichRequest(){
  try{
    return which.sync('request');
  }catch(ex){
    console.error( chalk.red(symbols.err) + 'can not find `request` on your PATH.')
    console.error('');
    console.error( chalk.yellow('!') + 'You should set up node and npm first, go at ');
    console.error( chalk.yellow('!') + 'http://nodejs.org');
    console.error('');
    console.error( chalk.yellow('!') + 'then you can type in ');
    console.error( chalk.yellow('!') + 'npm i https://github.com/maboiteaspam/request-cli.git -g');
    console.error('');

    return false;
  }
}

function whichBrowserify(){
  try{
    return which.sync('browserify');
  }catch(ex){
    console.error( chalk.red(symbols.err) + 'can not find `browserify` on your PATH.')
    console.error('');
    console.error( chalk.yellow('!') + 'You should set up node and npm first, go at ');
    console.error( chalk.yellow('!') + 'http://nodejs.org');
    console.error('');
    console.error( chalk.yellow('!') + 'then you can type in ');
    console.error( chalk.yellow('!') + 'npm i browserify -g');
    console.error('');

    return false;
  }
}

function whichExorcist(){
  try{
    return which.sync('exorcist');
  }catch(ex){
    console.error( chalk.red(symbols.err) + 'can not find `exorcist` on your PATH.')
    console.error('');
    console.error( chalk.yellow('!') + 'You should set up node and npm first, go at ');
    console.error( chalk.yellow('!') + 'http://nodejs.org');
    console.error('');
    console.error( chalk.yellow('!') + 'then you can type in ');
    console.error( chalk.yellow('!') + 'npm i https://github.com/maboiteaspam/exorcist.git -g');
    console.error('');

    return false;
  }
}

function whichWatchy(){
  try{
    return which.sync('watchy');
  }catch(ex){
    console.error( chalk.red(symbols.err) + 'can not find `watchy` on your PATH.')
    console.error('');
    console.error( chalk.yellow('!') + 'You should set up node and npm first, go at ');
    console.error( chalk.yellow('!') + 'http://nodejs.org');
    console.error('');
    console.error( chalk.yellow('!') + 'then you can type in ');
    console.error( chalk.yellow('!') + 'npm i watchy -g');
    console.error('');

    return false;
  }
}

function fileAppend(filePath, content){
  if(fs.existsSync(filePath)){
    var g = fs.readFileSync(filePath).toString();
    if(!g.match(content)){
      g+='\n' + content+'\n';
      fs.writeFileSync(filePath, g);
    }
  }else{
    fs.writeFileSync(filePath, content+'\n');
  }
}