# demandware simple assets builder

Straight forward solution to watch, build, then push your JS and CSS built assets on your VM instance into the current active version.

In short, it replaces all the grunt / gulp tool suite provided in favor of command lines calls.

# Install

`npm i git+ssh://git@bitbucket.org:clementauger/demandware-simpleassetbuilder.git -g`

# Setup

This software comes in with an handy JIT setup helper.

Here is the list of required programs,

    - ruby
    - nodejs
    - gem install sass
    - npm i browserify watchy uglify-js postcss-cli autoprefixer -g 
    - npm i https://github.com/maboiteaspam/exorcist.git -g 
    - npm i https://github.com/maboiteaspam/request.git -g 

# Usage

On root folder of your cartridges, open a command line, then run 

`ddware-sab`

That's it.

System will start to watch for changes in configured folders.

Every time a file change is detected, the appropriate build process is run.

Right after, the result of the build is pushed via webdav on your active code version, in the appropriate path.

##### CLI Usage

```
  Usage: ddware-sab [options]

  Options:

    -h, --help             output usage information
    -V, --version          output the version number
    -p, --path <path>      CWD path the program will work in.
    -o, --only <only>      css|js
    -c, --config <config>  Configuration file name, default to dev
```

# Configuration

It is possible to configure most of the command line tools involved during the build process.

Within your `package.json` file, add keys such 

- `build.dev.vars`
- `build.dev.bin`

##### Default configuration values

`build.dev.vars`
```js
{
  projectPath: cwdPath,
  thisBinPath: __dirname,
  vmOpts: vmOpts,
  version: '',
  vmBaseUrl: 'https://<%=vmOpts.user%>:<%=vmOpts.password%>@<%=vmOpts.host%>/on/demandware.servlet/webdav/Sites/Cartridges/<%=version%>',
  path: path,
  
  js:{
    watch: "",
    source: "",
    target: "",
    targetMap: "",
    tempPath: ""
  },
  
  css:{
    watch: "",
    target: "",
    targetMap: "",
    tempPath: ""
  }
}
```

`build.dev.bin`
```js
{
  scss: '--sourcemap=auto --style=compressed <%=srcCSSPath%> <%=dstCSSPath%>',
  autoprefix: '--use autoprefixer --autoprefixer.browsers "last 2 versions, ie > 7" -o <%=dstCSSPath%> <%=dstCSSPath%>',

  browserify: '<%=srcJSPath%> -t [ <%=thisBinPath%>/node_modules/aliasify --configDir <%=projectPath%>] --debug -o <%=tempPath%>',
  exorcist: '<%=tempPath%>.map -o <%=tempPath%>.tmp -i <%=tempPath%> -u <%=path.basename(tempPath)%>.map',
  uglify: '<%=tempPath%>.tmp -v --mangle --source-map <%=dstJSPath%>.map --in-source-map <%=tempPath%>.map --source-map-url <%=path.basename(dstJSPath)%>.map -o <%=dstJSPath%>',

  push: '<%=vmBaseUrl%>/<%=currentPath%> -T <%=projectPath%>/<%=currentPath%>'
}
```

It makes use of `_.template` engine to resolve binary command line invocations.

The list of available variables are in `build.dev.vars`

Note that some variable such version`, `projectPath`, `thisBinPath`, `vmOpts`, `tempPath` may be not re writable because they are set __Just In Time__ during the build.

As an example `version` field is available only after the program made request / parse operation of the `.version` file located on your remote VM.

Note also that variable are processed in order, thus it is possible to make variable using variables. In such case, required variables must be defined higher in the list of variables.

# Importing between cartridges

It is possible import and share dependencies between cartridges.

##### JS

It relies on browserify transform function and aliasify module.

In the `package.json` file, add a new key `aliasify.aliases`

```json
{
    "aliasify": {
        "aliases": {
            "bar": "./app_brand_a/cartridge/js/bar"
        }
    }
}
```

There you can reference alias module and their absolute path.

It will be managed and resolved properly.

All the files path referenced into the `package.json` are resolved 
according to the location of that `package.json` file.


##### CSS

No nice solution here, so far. 

Simply reference the scss file to include from the other cartridges within your import rules

```
    @import "../the/path/to/the/file/to/import";
```

# What does it do to my source ?

##### JS
- browersify with source map
- exorcize the resulting file to extract the inlined source map
- uglify the final result, trying to keep source map coherent and reduce the size (mangle+minify)

##### CSS
- scss the stylesheets, with sourcemap and compressed output
- autoprefix the resulting file

# Notes

Unlike gulp/grunt, this does not aim to be highly configurable, rather than provide a working and ready-to-go solution that just works.

If you need more personalization, well you have an example here, go on by yourself or keep using grunt/gulp.

There is no test suite for thar repo, if you need to debug it, you may like to do that 

`SET DEBUG=demand* && node index.js -p <path to the project cartridges>`

`export DEBUG=demand* && node index.js -p <path to the project cartridges>`

# Todo

- empty (x) for now on.


# See also

https://github.com/postcss/autoprefixer
https://github.com/code42day/postcss-cli
https://github.com/jakubpawlowicz/clean-css
https://github.com/thlorenz/exorcist
https://github.com/maboiteaspam/exorcist
https://github.com/mishoo/UglifyJS2
https://github.com/caseywebdev/watchy
https://github.com/substack/node-browserify
https://github.com/maboiteaspam/request-cli
http://sass-lang.com/install
https://github.com/ai/browserslist

They are all awesome. Node is awesome. NPM is awesome.
